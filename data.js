var APP_DATA = {
  "scenes": [
    {
      "id": "0-1",
      "name": "1",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.25460557015406415,
          "pitch": 0.32367510482870365,
          "rotation": 11.780972450961727,
          "target": "1-2"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -3.0906473284023086,
          "pitch": -0.2545346416190135,
          "id":"#tooltip1"
        }
      ],
      "picture":
      [
        {
          "yaw": 2.332,
          "pitch":0.013,
          "id":"#NFT1",
          "skew":"skew(0deg,0deg)",
        },

        {
          "yaw": 2.8158,
          "pitch":0.017,
          "id":"#NFT2",
          "skew":"skew(0deg,2deg)",
        },
        {
          "yaw": 3.5124115563955277,
          "pitch":  0.01635741110222514,
          "id":"#NFT3",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw":  3.994,
          "pitch": 0.01378695413934899,
          "id":"#NFT4",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 5.1176029481141825,
          "pitch": 0.0101100,
          "id":"#NFT5",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 4.4537,
          "pitch":  0.01419,
          "id":"#NFT6",
          "skew":"rotate(-1deg)",
        },
        {
          "yaw": 1.869827474434887,
          "pitch":  0.01000,
          "id":"#NFT7",
          "skew":"rotate(1.9deg)",
        },
        {
          "yaw": 1.213,
          "pitch":  0.01,
          "id":"#NFT8",
          "skew":"rotate(1.7deg)", 
        },
      ]
    },
    {
      "id": "1-2",
      "name": "2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 3.1398074513820857,
        "pitch": 0.1285604978995245,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": 0.7728709222189671,
          "pitch": 0.3073529989340198,
          "rotation": 0.7853981633974483,
          "target": "0-1"
        },
        {
          "yaw": -0.7903955978336832,
          "pitch": 0.3029063195114965,
          "rotation": 5.497787143782138,
          "target": "2-3"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.50457501438129,
          "pitch": -0.27326948187850775,
          "id":"#tooltip2"
        }
      ],
      "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT10",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT11",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT12",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT13",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT14",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT15",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]
    },
    {
      "id": "2-3",
      "name": "3",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -2.9933473020906067,
        "pitch": 0.0004552783087063972,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": -0.8407328584837153,
          "pitch": 0.3062627876612005,
          "rotation": 5.497787143782138,
          "target": "3-4"
        },
        {
          "yaw": 0.856529333226657,
          "pitch": 0.31363882745203675,
          "rotation": 0.7853981633974483,
          "target": "1-2"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.4951963784399993,
          "pitch": -0.26453910079337994,
         "id": "#tooltip3"
        }
      ],
      "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT17",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT18",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT19",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT20",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT21",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT22",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]  
    },
    {
      "id": "3-4",
      "name": "4",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 1.7672373446251495,
        "pitch": -0.03306032265862413,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": 0.3551167720203452,
          "pitch": 0.29890916490672126,
          "rotation": 0.7853981633974483,
          "target": "2-3"
        },
        {
          "yaw": -0.2686933787463648,
          "pitch": 0.2990447642047638,
          "rotation": 5.497787143782138,
          "target": "4-5"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -3.1187882121429844,
          "pitch": -0.22516361825015174,
          "id": "#tooltip4"
        }
      ], "picture":
      [
        {
          "yaw": 2.332,
          "pitch":0.013,
          "id":"#NFT22",
          "skew":"skew(0deg,0deg)",
        },

        {
          "yaw": 2.8158,
          "pitch":0.017,
          "id":"#NFT23",
          "skew":"skew(0deg,2deg)",
        },
        {
          "yaw": 3.5124115563955277,
          "pitch":  0.01635741110222514,
          "id":"#NFT24",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw":  3.994,
          "pitch": 0.01378695413934899,
          "id":"#NFT25",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 5.1176029481141825,
          "pitch": 0.0101100,
          "id":"#NFT26",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 4.4537,
          "pitch":  0.01419,
          "id":"#NFT27",
          "skew":"rotate(-1deg)",
        },
        {
          "yaw": 1.869827474434887,
          "pitch":  0.01000,
          "id":"#NFT28",
          "skew":"rotate(1.9deg)",
        },
        {
          "yaw": 1.213,
          "pitch":  0.01,
          "id":"#NFT29",
          "skew":"rotate(1.7deg)", 
        },
      ]
    },
    {
      "id": "4-5",
      "name": "5",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 3.0994007784027584,
        "pitch": 0.08108206346379632,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": 0.8701799184606536,
          "pitch": 0.30357441423558207,
          "rotation": 0.7853981633974483,
          "target": "3-4"
        },
        {
          "yaw": -0.8682834758672424,
          "pitch": 0.2953312103376362,
          "rotation": 5.497787143782138,
          "target": "5-6"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.484683581944938,
          "pitch": -0.26335918010020976,
          "id": "#tooltip5"
        }
      ], "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT31",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT32",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT33",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT34",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT35",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT36",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]  
    },
    {
      "id": "5-6",
      "name": "6",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -3.1014448882339902,
        "pitch": -0.015704153087508388,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": -0.844384386704979,
          "pitch": 0.2996860917849897,
          "rotation": 11.780972450961727,
          "target": "6-7"
        },
        {
          "yaw": 0.8476826302810121,
          "pitch": 0.3046733087907576,
          "rotation": 7.0685834705770345,
          "target": "4-5"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.4855308515698953,
          "pitch": -0.25602538302662836,
          "id": "#tooltip6"
        }
      ], "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT38",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT39",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT40",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT41",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT42",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT43",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]
    },
    {
      "id": "6-7",
      "name": "7",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 1.7573872017862904,
        "pitch": 0.053873969619687045,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": -0.2892884696404874,
          "pitch": 0.30210126716881547,
          "rotation": 5.497787143782138,
          "target": "7-8"
        },
        {
          "yaw": 0.37201369027330244,
          "pitch": 0.3068794389979761,
          "rotation": 0.7853981633974483,
          "target": "5-6"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.555025149282093,
          "pitch": -0.22209050906139183,
          "id": "#tooltip7"
        }
      ],"picture":
      [
        {
          "yaw": 2.332,
          "pitch":0.013,
          "id":"#NFT44",
          "skew":"skew(0deg,0deg)",
        },

        {
          "yaw": 2.8158,
          "pitch":0.017,
          "id":"#NFT45",
          "skew":"skew(0deg,2deg)",
        },
        {
          "yaw": 3.5124115563955277,
          "pitch":  0.01635741110222514,
          "id":"#NFT46",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw":  3.994,
          "pitch": 0.01378695413934899,
          "id":"#NFT47",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 5.1176029481141825,
          "pitch": 0.0101100,
          "id":"#NFT48",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 4.4537,
          "pitch":  0.01419,
          "id":"#NFT49",
          "skew":"rotate(-1deg)",
        },
        {
          "yaw": 1.869827474434887,
          "pitch":  0.01000,
          "id":"#NFT50",
          "skew":"rotate(1.9deg)",
        },
        {
          "yaw": 1.213,
          "pitch":  0.01,
          "id":"#NFT51",
          "skew":"rotate(1.7deg)", 
        },
      ]
    },
    {
      "id": "7-8",
      "name": "8",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -3.139531289072675,
        "pitch": 0.05750655381446457,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": -0.8399494142356971,
          "pitch": 0.3102011129676985,
          "rotation": 5.497787143782138,
          "target": "8-9"
        },
        {
          "yaw": 0.8377157117467693,
          "pitch": 0.31698103302537817,
          "rotation": 0.7853981633974483,
          "target": "6-7"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.50335466829757,
          "pitch": -0.25939900606460675,
          "id": "#tooltip8"
        }
      ], "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT53",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT54",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT55",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT56",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT57",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT58",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]
    },
    {
      "id": "8-9",
      "name": "9",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -3.1252974493878405,
        "pitch": 0.001987269365841371,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": -0.8388361344297408,
          "pitch": 0.31812176581887996,
          "rotation": 5.497787143782138,
          "target": "9-10"
        },
        {
          "yaw": 0.8454876515250742,
          "pitch": 0.3218976987923341,
          "rotation": 0.7853981633974483,
          "target": "7-8"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.49187159560734,
          "pitch": -0.2718443038277769,
          "id": "#tooltip9"
        }
      ], "picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT60",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT61",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT62",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT63",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT64",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT65",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]
    },
    {
      "id": "9-10",
      "name": "10",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": 2.0306729920497606,
        "pitch": 0.07433845338695377,
        "fov": 1.4108951493785415
      },
      "linkHotspots": [
        {
          "yaw": 0.31550374367028766,
          "pitch": 0.33527118193562444,
          "rotation": 0.7853981633974483,
          "target": "8-9"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 1.5417235523726252,
          "pitch": -0.22744286834080896,
          "id": "#tooltip10"
        }
      ], "picture":
      [
        {
          "yaw": 2.332,
          "pitch":0.013,
          "id":"#NFT66",
          "skew":"skew(0deg,0deg)",
        },

        {
          "yaw": 2.8158,
          "pitch":0.017,
          "id":"#NFT67",
          "skew":"skew(0deg,2deg)",
        },
        {
          "yaw": 3.5124115563955277,
          "pitch":  0.01635741110222514,
          "id":"#NFT68",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw":  3.994,
          "pitch": 0.01378695413934899,
          "id":"#NFT69",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 5.1176029481141825,
          "pitch": 0.0101100,
          "id":"#NFT70",
          "skew":"skew(0deg,0deg)",
        },
        {
          "yaw": 4.4537,
          "pitch":  0.01419,
          "id":"#NFT71",
          "skew":"rotate(-1deg)",
        },
        {
          "yaw": 1.869827474434887,
          "pitch":  0.01000,
          "id":"#NFT72",
          "skew":"rotate(1.9deg)",
        },
        {
          "yaw": 1.213,
          "pitch":  0.01,
          "id":"#NFT73",
          "skew":"rotate(1.7deg)", 
        },
      ]
    },
    {
      "id": "10-11",
      "name": "11",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 2048,
      "initialViewParameters": {
        "yaw": -3.0439619573113514,
        "pitch": 0.049026635466679735,
        "fov": 1.3330501835974622
      },
      "linkHotspots": [
        {
          "yaw": 0.8400447695933053,
          "pitch": 0.2678240533995595,
          "rotation": 7.0685834705770345,
          "target": "9-10"
        },
        {
          "yaw": -0.8550642073197512,
          "pitch": 0.26735458399328493,
          "rotation": 5.497787143782138,
          "target": "0-1"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 2.327416178712424,
          "pitch": -0.2321125585608339,
          "id":"#tooltip11"
        }
      ],"picture":
      [


        {
          "yaw": 2.209827474434887,
          "pitch":  -0.00019215547101717,
          "id":"#NFT75",
	"skew":"rotate(-2deg)",
  
        },
        {
          "yaw": 2.904115563955277,
          "pitch":  0.00835741110222514,
          "id":"#NFT51",
	"skew":"skew(0deg, -2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw":  3.512196818991144,
          "pitch": 0.0078695413934899,
          "id":"#NFT52",
	"skew":"skew(0deg,1deg)"
        },
        {
          "yaw": 4.1676029481141825,
          "pitch": 0.004419163220323696,
          "id":"#NFT53",
	"skew":"skew(0deg,0deg)"
        },
        {
          "yaw": 4.7237707291474635,
          "pitch":  -0.00141928016877586,
          "id":"#NFT54",
	"skew":"skew(-2deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
        {
          "yaw": 1.625827474434887,
          "pitch":  0.00229215547101717,
          "id":"#NFT55",
	"skew":"skew(0deg,-2deg)",
  "rotate":"rotate(-2deg, -2deg)"
        },
      ]
    }
  ],
  "name": "J69-1it",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
